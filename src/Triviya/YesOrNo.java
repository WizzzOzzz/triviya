/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triviya;

/**
 *
 * @author oriwi_000
 */
public class YesOrNo extends MultipleOptions {
    
    public YesOrNo(){
        super(1);
        boolean sel =true;
        String st="";
        int type;
        while (sel){
            System.out.println("Please Choose the question subject");
            st = s.nextLine();
            sub = st.replaceAll("[^a-z,^A-Z]", "");
        }
        sel=true;
        while (sel){
            System.out.println("Please Choose the question body");
            st = s.nextLine();
            body = st.replaceAll("[^a-z,^A-z]", "");
        }

        while (sel){
            System.out.println("Please Choose the question answer (yes , no");
            st = s.nextLine();
            if (st.equals("cancel"))
                return;
            if (!(st.equals("yes") || st.equals("no"))){
                System.out.println("You have to choose one of the following options");
                continue;
            }
            body = st.replaceAll("[^a-z,^A-z]", "");
        }
        sel=true;
        int d;
        String c;
        while (sel){
            System.out.println("Please Choose the question difficulty (0 - easy, 1 - medium, 2 - hard");
            c=s.nextLine();
            if (c.equals("cancel"))
                return;
            d = Integer.parseInt(c);
            dif=d;
        }
        questionType="Yes or no";
    }
}
