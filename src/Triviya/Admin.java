/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triviya;

import com.sun.xml.internal.ws.encoding.soap.SerializationException;
import java.io.*;
import java.util.*;

/**
 *
 * @author oriwi_000
 */
public class Admin extends User implements Serializable {

    private String userName;
    private String pass;
    private transient Scanner s;
    HashMap<String, ArrayList<Question>> hm;

    public Admin() throws IOException, ClassNotFoundException, NotSerializableException {
        String opt = "";
        while (true) {
            s = new Scanner(System.in);
            System.out.println("What would you like to do? (add, delete, exit, change(change user), *)");
            opt = s.nextLine();
            switch (opt) {
                case "add":
                    createQuestion();
                    break;
                case "delete":
                    deleteQuestion();
                    break;
                case "exit":
                    System.exit(0);
                case "change":
                case "change user":
                    return;
                case "start":
                    startGame();
                default:
                    System.out.println("Only the following options are available");
                    break;
            }
        }
    }

    public void writeToFile(HashMap<String, ArrayList<Question>> hm) {
        ObjectOutputStream obj;
        try {

            obj = new ObjectOutputStream(new FileOutputStream("..//Questions"));
            obj.flush();
            obj.writeObject(hm);
            obj.flush();
            obj.close();

        } catch (IOException e) {
            System.out.println(e.getMessage()/*"file not found write to file"*/);
        }
    }

    public HashMap<String, ArrayList<Question>> readFromFile() throws ClassNotFoundException {
        HashMap<String, ArrayList<Question>> hm;
        ObjectInputStream obj;
        try {
            obj = new ObjectInputStream(new FileInputStream("..//Questions"));
            hm = (HashMap<String, ArrayList<Question>>) obj.readObject();
            obj.close();
        } catch (IOException e) {
            System.out.println("The file does not exist\nThe system will create it");
            hm = new HashMap<String, ArrayList<Question>>();
        }

        return hm;
    }

    public void deleteQuestion() throws ClassNotFoundException {
        hm = readFromFile();
        Iterator it = hm.entrySet().iterator();
        HashMap.Entry<String, ArrayList<Question>> pair = null;
        int i = 0;
        while (it.hasNext()) {
            pair = (HashMap.Entry<String, ArrayList<Question>>) it.next();
            System.out.println("For Category " + pair.getKey());
            for (Question q : pair.getValue()) {
                System.out.println(q.ask() + " " + i);
                i++;
            }
        }

        int qnumber;
        System.out.println("Please enter question number you would like to delete");
        qnumber = s.nextInt();
        i = 0;
        it = hm.entrySet().iterator();
        while (it.hasNext()) {
            pair = (HashMap.Entry<String, ArrayList<Question>>) it.next();
            System.out.println("For Category " + pair.getKey());
            ArrayList<Question> alq = pair.getValue();
            for (Question q : alq) {
                if (i == qnumber) {
                    pair.getValue().remove(q);
                    for (Question p : pair.getValue()){
                        System.out.println("New Hash");
                        System.out.println(q.ask() + " " + i);
                    }
                    break;
                }
            }
        }
        writeToFile(hm);
        
    }

    public void addQuestion(Question q) throws ClassNotFoundException {
        ArrayList<Question> arr = new ArrayList<>();
        hm = readFromFile();
        if (!(hm.containsKey(q.getSub()))) {
            arr.add(q);
            hm.put(q.getSub(), arr);
        } else {
            hm = readFromFile();
            hm.get(q.getSub()).add(q);
        }
        writeToFile(hm);
        System.out.println("Question has been added");

    }

    public void createQuestion() throws IOException, ClassNotFoundException {
        int type;

        String st;
        Question q;
        while (true) {
            System.out.println("Please choose what kind of question it is (0 - yes or no ,1 - multiple options, 2 - open qeustion)");
            type = s.nextInt();
            switch (type) {
                case 0:
                    q = new YesOrNo();
                    addQuestion(q);
                    return;
                case 1:
                    q = new MultipleOptions();
                    addQuestion(q);
                    return;
                case 2:
                    q = new OpenQuestion();
                    addQuestion(q);
                    return;
                default:
                    System.out.println("you have to choose one of the followed options");
                    break;
            }
        }
    }
}
