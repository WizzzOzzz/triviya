/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triviya;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author oriwi_000
 */
public class MultipleOptions extends Question {

    ArrayList<String> falseAns;

    public MultipleOptions(int i) {
        super(1);
    }

    public MultipleOptions() {
        super();
        String st = "";
        boolean sel = true;
        s = new Scanner(System.in);

        System.out.println("Please Choose the question answer");
        st = s.nextLine();
        body = st;
        falseAns = new ArrayList<>();
        while (true) {
            System.out.println("Please Choose the question false answer");
            st = s.nextLine();
            falseAns.add(st);
            if (sel) {
                System.out.println("Do you wanna add more false questions?");
                String c = s.nextLine();
                falseAns.add(c);
                if (c.equals("n")) {
                    break;
                }
            }
        }
        questionType = "Multiple answers";
    }
}
