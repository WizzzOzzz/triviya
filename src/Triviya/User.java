/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triviya;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.*;

/**
 *
 * @author oriwi_000
 */
public class User {
    
     public void startGame() throws FileNotFoundException, IOException, ClassNotFoundException{
        System.out.println("The Session has began");
        HashMap<String, ArrayList<Question>> hm;    
        try{
            ObjectInputStream in = new ObjectInputStream(new FileInputStream("../Questions"));
            hm = (HashMap<String,ArrayList<Question>>)in.readObject();
            in.close();
        }
        catch (Exception c){
            System.out.println("The file that contains the questions might be missing, try and add a question");
            return;
        }
        Iterator it = hm.entrySet().iterator();
        ArrayList<Question> arr;
        Scanner s = new Scanner(System.in);
        while (it.hasNext()){
            Map.Entry<String, ArrayList<Question>> e = (Map.Entry<String, ArrayList<Question>>)it.next();
            arr= e.getValue();
            for (Question q: arr){
                String ans="";
                while (!q.checkAns(ans)){
                    System.out.println(q.ask()+"?");
                    s = new Scanner(System.in);
                    ans = s.nextLine();
                    if (ans.equals("exit"))
                        System.exit(1);
                }
                System.out.println("correct!\nNext Question:");
            }
        }

    }
}
