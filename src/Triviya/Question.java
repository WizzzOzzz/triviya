/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Triviya;

import java.util.Scanner;
import java.io.*;

/**
 *
 * @author oriwi_000
 */
public abstract class Question implements Serializable {

    protected int dif; // 0 for easy 1 for med 2 for hard
    protected String sub; // question's subject
    protected transient Scanner s = new Scanner(System.in);
    protected String questionType;
    protected String body;
    protected String correctAns;
    protected String[] falseAns;

    public Question(int i) {
        System.out.println("please choose the question difficulty (0 for easy 1 for medium 2 for hard)");
        dif = s.nextInt();
    }

    public Question() {
        this(1);
        String st = "";
        s = new Scanner(System.in);
        System.out.println("Please Choose the question subject");
        st = s.nextLine();
        sub = st;
        System.out.println("Please Choose the question body");
        st = s.nextLine();
        body = st;
    }

    public String getType() {
        return questionType;
    }

    public int getDif() {
        return dif;
    }

    public String ask() {
        return body;
    }

    public String getInfo() {
        return "Subject: " + sub + "\nBody: " + body + "\nType: " + questionType;
    }

    public String getSub() {
        return sub;
    }

    public boolean checkAns(String ans) {
        if (ans.equals("")) {
            return false;
        }
        return correctAns.equals(ans);
    }
}
